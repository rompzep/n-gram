# -*- coding: utf-8 -*-
#
# Romain Poupin - M1 Informatique - Groupe Alternant
#
# /!\ Python 3.6+
# Lancer: python3 ngram.py


from ngram_utils import *


def main():
    # Apprentissage
    tri, bi, uni = parse()
    c_tri, c_bi, c_uni = count(tri, bi, uni)
    p_tri, p_bi, p_uni = estimate(c_tri, c_bi, c_uni, len(uni))

    # Test
    test(p_tri, p_bi, p_uni)

    # Propose les mots les plus probables à chaque frappe au cours d'une saisie
    # (Entrer pour arrêter)
    sentence = ""
    print(">>> écrivez <<<")
    while True:
        c = getchar()
        sentence += c
        print(sentence)
        if c.strip() == '':
            if c != ' ':
                break
        print(compute_sentence(sentence, p_tri, p_bi, p_uni))


# Éxécute la fonction main au lancement du programme
if __name__ == '__main__':
    main()
