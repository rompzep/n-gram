# -*- coding: utf-8 -*-
#
# Romain Poupin - M1 Informatique - Groupe Alternant
#
# /!\ Python 3.6+


from collections import Counter
from itertools import islice
from math import log10
import sys
import tty
import termios
TRAIN_CORPUS = "trainTP_SRL.txt"
TEST_CORPUS = TRAIN_CORPUS
UNIGRAM = 1
BIGRAM = 2
TRIGRAM = 3
LINES_TO_LEARN = 500  # Nombre des premières lignes du corpus à étudier
LINES_TO_TEST = 500  # Nombre des premières lignes du corpus à tester


"""Parse notre corpus d'apprentissage pour dégager nos n-grammes

Ces n-grammes sont stockés dans des listes où chaque séquence est une string.
Exemple pour un modèle bi-gramme avec le corpus suivant :

Je suis content
Je suis fou et confiné

bi = ["Je suis", "suis content", "Je suis", "suis fou", "fou et", "et confiné"]

RETURNS
    tri, bi, uni : string lists

OPTIONS
    corpus: fichier
        corpus d'apprentissage
    (default=TRAIN_CORPUS)

    tri, bi, uni: string lists
        nos modèles n-grammes
        (il suffit d'appeler à nouveau cette fonction avec des
        n-grammes existants pour les enrichir depuis un autre corpus)
    (default=[])

    lines: int
        lignes du corpus à apprendre (en partant du début)
    (default=LINES_TO_LEARN)
"""


def parse(corpus=TRAIN_CORPUS, tri=[], bi=[], uni=[], lines=LINES_TO_LEARN):
    print("parse les {} premières lignes du corpus {} en n-grammes...".format(lines, corpus))
    with open(corpus, "r", encoding="latin-1") as file:
        head = [next(file) for x in range(lines)]
        for line in head:
            mots = line.strip('\n').split()
            for mot in range(0, len(mots) - 2):
                tri.append(" ".join(mots[mot:mot + 3]))
            for mot in range(0, len(mots) - 1):
                bi.append(" ".join(mots[mot:mot + 2]))
            for mot in range(0, len(mots)):
                uni.append(" ".join(mots[mot:mot + 1]))
    return tri, bi, uni


"""Compte les occurences de chacune des séquences des n-grammes

Ces compteurs sont des dictionnaires {séquence: occurences}
Exemple pour les bi-grammes :

c_bi = {"Je suis": 2, "suis content": 1, "suis fou": 1, "fou et": 1, "et confiné": 1}

RETURNS
    c_tri, c_bi, c_uni: dicts {string: int}

ARGUMENTS
    tri, bi, uni: string lists
        nos modèles n-grammes
"""


def count(tri, bi, uni):
    print("compte les occurences de chaque séquence des n-grammes...")
    c_uni = Counter(uni)
    c_bi = Counter(bi)
    c_tri = Counter(tri)
    return c_tri, c_bi, c_uni


"""Calcule les probabilités des N-grammes avec la méthode de lissage additif

On réalise ce lissage pour obtenir des probabilités non nulles
même pour des séquences non vues dans le corpus d'apprentissage.

Padd(wi | wi-n) =
C(wi-n, wi) + smoothing
-----------------
C(wi-n) + smoothing * total_words

Aussi, nous encadrons les probabilités dans des logs pour palier
aux problèmes de précision des divisions des nombres décimaux.
"""


def compute_p_tri(tri_key, c_tri, c_bi, smoothing, total_words):
    bi_key = ' '.join(tri_key.split(' ')[0:2])
    return float(log10(
        (smoothing + c_tri[tri_key]) / (smoothing * total_words + c_bi[bi_key])))


def compute_p_bi(bi_key, c_bi, c_uni, smoothing, total_words):
    uni_key = ' '.join(bi_key.split(' ')[0:1])
    return float(log10(
        (smoothing + c_bi[bi_key]) / (smoothing * total_words + c_uni[uni_key])))


def compute_p_uni(uni_key, c_uni, smoothing, total_words):
    return float(log10(
        (smoothing + c_uni[uni_key]) / (smoothing * total_words + total_words)))


"""
RETURNS
    p_tri, p_bi, p_uni: dicts {string: float}

ARGUMENTS
    c_tri, c_bi, c_uni: dicts {string: int}
        nos compteurs d'occurences des séquences des n-grammes

    total_words: int
        nombre de mots de notre vocabulaire

OPTIONS
    smoothing: float
        facteur de lissage
    (default=0.1)
"""


def estimate(c_tri, c_bi, c_uni, total_words, smoothing=0.1):
    print("calcule les probabilités de chaque séquence des n-grammes...\n(smoothing: {}, total_words: {})".format(smoothing, total_words))
    p_tri = {}
    p_bi = {}
    p_uni = {}
    for tri_key in c_tri:
        p_tri[tri_key] = compute_p_tri(
            tri_key, c_tri, c_bi, smoothing, total_words)
    for bi_key in c_bi:
        p_bi[bi_key] = compute_p_bi(
            bi_key, c_bi, c_uni, smoothing, total_words)
    for uni_key in c_uni:
        p_uni[uni_key] = compute_p_uni(uni_key, c_uni, smoothing, total_words)
    return p_tri, p_bi, p_uni


"""Retourne les <top> mots les plus probables d'une séquence

1°  On récupère tous les candidats possibles en conservant que
    ceux qui matchent avec la portion du mot à prédire, s'il y en a une (word_to_complete).
2°  Puis les trie par probabilité décroissante.
3°  Et retourne une liste des <top> plus probables.

RETURNS
    liste des candidats: string list

ARGUMENTS
    gram_type: int
        modèle n-gramme à utiliser
    (UNIGRAM, BIGRAM ou TRIGRAM)

    gram_proba: dicts {string, float}
        probabilités des séquences des n-grammes

OPTIONS
    previous_words: string
        séquence précédent le mot à prédire
    (default='')

    word_to_complete: string
        début du mot à prédire
    (default='')

    top: int
        nombre de candidats max
    (default=5)
"""


def predict_word(gram_type, gram_proba, previous_words='', word_to_complete='', top=5):
    if gram_type == UNIGRAM:
        candidates = {s: p for s, p in gram_proba.items()
                      if s.startswith(word_to_complete)}
    elif gram_type == BIGRAM:
        candidates = {s: p for s, p in gram_proba.items()
                      if s.split(" ")[0] == previous_words
                      and (s.split(" ")[1]).startswith(word_to_complete)}
    elif gram_type == TRIGRAM:
        candidates = {s: p for s, p in gram_proba.items()
                      if " ".join(s.split(" ")[0:2]) == previous_words
                      and (s.split(" ")[2]).startswith(word_to_complete)}

    candidates = {s: p for s, p in sorted(
        candidates.items(), key=lambda item: item[1])}

    return [candidate.split(" ")[-1] for candidate, n in zip(candidates, range(0, top))]


"""Détermine les paramètres de prédiction avant de la déléguer

On adapte la stratégie de prédiction (quel modèle n-gramme on utilise)
en fonction de la longueur de la phrase.

Aussi, on filtre les prédictions pour correspondre
au mot en cours de frappe par l'utilisateur.

RETURNS
    les candidats les plus probables: string list

ARGUMENTS
    sentence: string
        phrase dont il faut prédire le mot suivant

    p_: dicts {string: float}
        probabilités des séquences des modèles n-grammes
"""


def compute_sentence(sentence, p_tri, p_bi, p_uni):
    words = sentence.split()
    len_words = len(words)
    if sentence.split(' ')[-1] == '':  # new word mode
        if len_words == 0:
            prediction = predict_word(UNIGRAM, p_uni)
        elif len_words == 1:
            prediction = predict_word(BIGRAM, p_bi, words[0])
        elif len_words >= 2:
            prediction = predict_word(TRIGRAM, p_tri, " ".join(words[-2:]))
    else:                           # complete word mode
        if len_words == 1:
            prediction = predict_word(
                UNIGRAM, p_uni, word_to_complete=words[0])
        elif len_words == 2:
            prediction = predict_word(BIGRAM, p_bi, words[-2], words[-1])
        elif len_words >= 3:
            prediction = predict_word(
                TRIGRAM, p_tri, " ".join(words[-3:-1]), words[-1])
    return prediction


"""Teste nos modèles n-grammes sur les mêmes lignes du même corpus d'apprentissage

Pourquoi encadrer la prédiction ?
Il se peut qu'il n'y ait aucun candidat si la séquence précédant
le mot à prédire n'a jamais été rencontrée lors de l'apprentissage.

ARGUMENTS
    p_: dicts {string: float}
        probabilités des séquences des modèles n-grammes

OPTIONS
    corpus: file
        corpus à tester

    lines: int
        lignes du corpus à tester (en partant du début)
"""


def test(p_tri, p_bi, p_uni, corpus=TEST_CORPUS, lines=LINES_TO_TEST):
    print("Teste nos modèles n-grammes sur les {} premières lignes du corpus {}...".format(lines, corpus))
    finded = 0
    failed = 0
    with open(corpus, "r", encoding="latin-1") as file:
        head = [next(file) for x in range(lines)]
        for line in head:
            words = line.strip('\n').split()
            # print(words)
            sentence = ""
            for word, answer in zip(words[:-1], words[1:]):
                sentence += word + " "
                # print(sentence + "?")
                try:
                    best_candidate = compute_sentence(
                        sentence, p_tri, p_bi, p_uni)[0]
                except:
                    best_candidate = None
                if best_candidate == answer:
                    finded += 1
                else:
                    failed += 1
                # print("meilleur candidat: {}".format(best_candidate))
                # print("réalité: " + answer)
            # print("\n")
        print("resultats: {}% de réussite\nsuccès = {}\néchecs = {}".format(
            round(float(finded / (finded + failed) * 100), 2), finded, failed))


"""Get a single character from stdin, Unix version

Source : https://gist.github.com/payne92/11090057
"""


def getchar():
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())  # Raw read
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch
